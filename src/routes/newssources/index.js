import React from 'react';
import Layout from '../../components/Layout';
import Source from './Source';
import fetch from '../../core/fetch';


export default {

  path : '/sources',

  async action(){

    const resp = await fetch('https://newsapi.org/v1/sources?language=en');
    let data = await resp.json();

    return {
        title : 'News sources',
        component : <Layout><Source sources={data.sources} /></Layout>
  }



  }

}
