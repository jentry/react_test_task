import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './source.css';

class Source extends React.Component {

  render(){

    return(
      <div className="sources">
      <div className={s.container}>
      <h1>News sources</h1>
    {this.props.sources.map((post, index) => (
    <div className={s.single} key={post.id}>
      <img src={post.urlsToLogos.small} alt="" />
  <p className="title"><a href={post.url}>{post.name}</a></p>
    <p>{post.description}</p>
    </div>
  ))
  }
  </div></div>
  )
  }

}

export default withStyles(s)(Source);
