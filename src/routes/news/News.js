import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './news.css';

class News extends React.Component {

  render(){

    return(
      <div className="news">
        <div className={s.container}>
      <h1>News api</h1>
    {this.props.articles.map((post, index) => (
        <div className={s.single}>
          <img src={post.urlToImage} alt="" />
          <p className="title"><a href={post.url}>{post.title}</a></p>
          <p>{post.description}</p>
          <p>{post.author}</p>
        </div>
      ))
    }
      </div></div>
  )
  }

}

export default withStyles(s)(News);
