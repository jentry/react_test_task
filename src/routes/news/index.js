import React from 'react';
import Layout from '../../components/Layout';
import News from './News';
import fetch from '../../core/fetch';

const key = '7c4c680ee7504cacaa3cc6968afcb3b0';

export default {

  path : '/',

  async action(){

    const resp = await fetch('https://newsapi.org/v1/articles?source=the-next-web&sortBy=latest&apiKey='+key);
    let data = await resp.json();

    return {
        title : 'News api',
        component : <Layout><News articles={data.articles} /></Layout>
  }



  }

}
